/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.cuckoo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hanam
 */
public class Cuckoo {
    private int key;
    private String value;
    private Cuckoo[] lookupTable1 = new Cuckoo[101]; // assume size = 101  
    private Cuckoo[] lookupTable2 = new Cuckoo[101]; // assume size = 101 

    public Cuckoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public Cuckoo() {
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public Cuckoo[] getLookupTable1() {
        return lookupTable1;
    }

    public Cuckoo[] getLookupTable2() {
        return lookupTable2;
    }
    
    
    public int hashTable1(int key) {
        return key % lookupTable1.length;
    }

    public int hashTable2(int key) {
        return (key / 97) % lookupTable2.length;
    }

    public void put(int key, String value) {

        if (lookupTable1[hashTable1(key)] != null && lookupTable1[hashTable1(key)].key == key) {
            lookupTable1[hashTable1(key)].key = key;
            lookupTable1[hashTable1(key)].value = value;
            return;
        }
        if (lookupTable2[hashTable2(key)] != null && lookupTable2[hashTable2(key)].key == key) {
            lookupTable2[hashTable2(key)].key = key;
            lookupTable2[hashTable2(key)].value = value;
            return;
        }
        int i = 0;
        
        //Repeat
        while (true) {
            if (i == 0) { //lookupTable1 (i=0)
                if (lookupTable1[hashTable1(key)] == null) {
                    lookupTable1[hashTable1(key)] = new Cuckoo();
                    lookupTable1[hashTable1(key)].key = key;
                    lookupTable1[hashTable1(key)].value = value;
                    return;
                }
            } else { //lookupTable2 (i==1)
                if (lookupTable2[hashTable2(key)] == null) {
                    lookupTable2[hashTable2(key)] = new Cuckoo();
                    lookupTable2[hashTable2(key)].key = key;
                    lookupTable2[hashTable2(key)].value = value;
                    return;
                }
            }

            //eviction
            if (i == 0) { //lookupTable1 (i=0)
                String tempValue = lookupTable1[hashTable1(key)].value;
                int tempKey = lookupTable1[hashTable1(key)].key;
                lookupTable1[hashTable1(key)].key = key;
                lookupTable1[hashTable1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else { ////lookupTable2 (i==1)
                String tempValue = lookupTable2[hashTable2(key)].value;
                int tempKey = lookupTable2[hashTable2(key)].key;
                lookupTable2[hashTable2(key)].key = key;
                lookupTable2[hashTable2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }
    
    public Cuckoo get(int key) {
        if (lookupTable1[hashTable1(key)] != null && lookupTable1[hashTable1(key)].key == key) {
            return lookupTable1[hashTable1(key)];
        }
        if (lookupTable2[hashTable2(key)] != null && lookupTable2[hashTable2(key)].key == key) {
            return lookupTable2[hashTable2(key)];
        }
        return null;
    }

    public Cuckoo delete(int key) {
        if (lookupTable1[hashTable1(key)] != null && lookupTable1[hashTable1(key)].key == key) {
            Cuckoo temp = lookupTable1[hashTable1(key)];
            lookupTable1[hashTable1(key)] = null;
            return temp;
        }
        if (lookupTable2[hashTable2(key)] != null && lookupTable2[hashTable2(key)].key == key) {
            Cuckoo temp = lookupTable2[hashTable2(key)];
            lookupTable2[hashTable2(key)] = null;
            return temp;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Cuckoo{" + "key=" + key + ", value=" + value + '}';
    }
    
    

}
