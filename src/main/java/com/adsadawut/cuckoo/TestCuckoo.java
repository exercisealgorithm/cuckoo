/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.cuckoo;

/**
 *
 * @author hanam
 */
public class TestCuckoo {
    public static void main(String[] args) {
        Cuckoo a = new Cuckoo();
        a.put(50, "King");
        a.put(150, "Kong");
        a.put(15, "ZAZA");
        a.put(115, "DDAD");
        a.put(100, "BBZ");
        a.put(100, "BBQ");
        System.out.println(a.get(50).toString());
        System.out.println(a.get(150).toString());
        System.out.println(a.get(15).toString());
        System.out.println(a.get(115).toString());
        System.out.println(a.get(100).toString()); //BBZ will Replace by BBQ
        a.delete(15);
        System.out.println(a.get(15));

    }
}
